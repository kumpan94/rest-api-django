from rest_framework.viewsets import ModelViewSet
from serializer import RoomSerializer
from models import Room

class RoomViewSet(ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer