# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 9, 20, 23, 14, 72854, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
